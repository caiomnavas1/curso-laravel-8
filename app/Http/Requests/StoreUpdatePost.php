<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdatePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;

        $rules = [
            'title' => "required|min:3|max:160|unique:posts,title,{$id},id",
            'content' => 'nullable|min:5|max:10000',
            'image' => 'required|image'
        ];

        if($this->method() == 'PUT'){
            $rules['image'] = 'nullable|image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'title.required' => 'Título é obrigatório',
            'title.min' => 'Título deve ter no mínimo 3 caracteres.',
            'title.max' => 'Título deve ter no máximo 160 caracteres.',
            'title.unique'  => 'Já existe um post com esse título.',
            'content.required' => 'Conteúdo é obrigatório',
            'content.min' => 'Conteúdo deve ter no mínimo 5 caracteres.',
            'content.max' => 'Conteúdo deve ter no máximo 10.000 caracteres.',
            'image.required' => 'Imagem é obrigatória',
            'image.image' => 'A imagem não é válida'
        ];
    }
}
