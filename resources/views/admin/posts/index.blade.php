@extends('dashboard')
@section('title', 'Listagem dos Posts')
@section('content')
        <div class="max-w-sm my-4 p-1 pr-0 flex items-center">
            <a class="btn btn-success" href="{{ route('posts.create') }}">Novo post</a>
        </div>

    @if(session('message'))
        <div class="alert alert-success" role="alert">
            {{ session('message') }}
        </div>
    @endif
    <form action="{{ route('posts.search') }}" method="post">
        @csrf
        <div class="max-w-sm my-4 p-1 pr-0 flex items-center">
            <input type="text" name="search" id="search" placeholder="Pesquisar" class="form-control" value="{{ $filters['search'] ?? old('search') }}">
            <button type="submit" class="btn btn-primary">Buscar</button>
        </div>
    </form>
    <table class="table">
        <thead>
        <tr>
            <th scope="col" class="text-center">Imagem</th>
            <th scope="col" class="text-center">Título</th>
            <th scope="col" class="text-center">Ações</th>
        </tr>
        </thead>
        <tbody>
        @foreach($posts as $post)
            <tr>
                <td class="align-middle">
                    <img src="{{ url("storage/{$post->image}") }}" alt="{{ $post->title }}" class="img-thumbnail img-fluid" style="max-width: 100px;">
                </td>
                <td class="align-middle">
                    {{ $post->title }}
                </td>
                <td class="align-middle text-center">
                    <a class="btn btn-primary" href="{{ route('posts.show', $post->id) }}">Ver</a>
                    <a class="btn btn-success" href="{{ route('posts.edit', $post->id) }}">Editar</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @if(isset($filters))
        {{ $posts->appends($filters)->links() }}
    @else
        {{ $posts->links() }}
    @endif
@endsection
