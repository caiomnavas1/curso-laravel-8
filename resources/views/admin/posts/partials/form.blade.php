<div class="max-w-sm my-2 p-1 pr-0 flex items-center">
    <a class="btn btn-warning" href="{{ route('posts.index') }}">Voltar</a>
</div>
@if($errors->any())
    <div class="alert alert-danger" role="alert">
        <ul style="margin-bottom: unset;">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ $action }}" method="post" enctype="multipart/form-data">
    @csrf
    @if(isset($method))
        @method( $method )
    @endif
    <div class="row">
        @if(isset($method))
        <div class="col-3">
            <p>
                <img src="{{ url("storage/{$post->image}") }}" alt="">
            </p>
        </div>
        @endif
        <div class="col-{{ isset($method) ? '9' : '12' }}">
            <input class="form-control" type="hidden" name="id" id="id" value="{{ $post->id ?? old('id') }}">
            <p>
                <input class="form-control" type="text" name="title" id="title" placeholder="Título"
                       value="{{ $post->title ?? old('title') }}">
            </p>
            <p>
                <textarea class="form-control" name="content" id="content" cols="30" rows="10"
                          placeholder="Conteúdo">{{ $post->content ?? old('content') }}</textarea>
            </p>
            <p>
                <input class="form-control" type="file" name="image" id="image"
                       value="{{ $post->image ?? old('image') }}">
            </p>
            <p class="text-center">
                <button class="btn btn-success" type="submit">
                    {{ isset($method) ? 'Editar' : 'Cadastrar' }} Post
                </button>
            </p>
        </div>
    </div>
</form>
