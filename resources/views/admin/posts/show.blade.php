@extends('dashboard')
@section('title', $post->title)
@section('content')
    <div class="max-w-sm my-2 p-1 pr-0 flex items-center">
        <a class="btn btn-warning" href="{{ route('posts.index') }}">Voltar</a>
    </div>
    <div class="row">
        <div class="col-3">
            <img src="{{ url("storage/{$post->image}") }}" alt="{{ $post->title }}">
        </div>
        <div class="col-9">
            <h1>{{ $post->title }}</h1>
            <p>{{ $post->content }}</p>
        </div>
    </div>

    <form action="{{ route('posts.destroy', $post->id) }}" method="post">
        @csrf
        <div class="max-w-sm my-2 p-1 pr-0 flex items-center">
            <input type="hidden" name="_method" value="DELETE">
            <button class="btn btn-danger" type="submit">Deletar o post {{ $post->title }}</button>
        </div>
    </form>
@endsection
