@extends('dashboard')
@section('title', 'Editar o Post ('.$post->title.')')
@section('content')
    @include('admin.posts.partials.form', [
        'action' => route('posts.update', $post->id),
        'method' => 'PUT'
     ])
@endsection
