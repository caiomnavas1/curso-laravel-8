@extends('dashboard')
@section('title', 'Novo Post')
@section('content')
    @include('admin.posts.partials.form', [
        'action' => route('posts.store')
     ])
@endsection
